DECLARE

CURSOR object_types is
select distinct object_type 
from user_objects;
				
CURSOR contrants is
select ref.OWNER, ref.TABLE_NAME, ref.CONSTRAINT_NAME
from user_constraints ref
where ref.constraint_type in ('R')
and (ref.TABLE_NAME like 'BR_%' or ref.TABLE_NAME like 'BRFH%')
order by ref.OWNER, ref.TABLE_NAME;
			
CURSOR tablelist( q_object_type in user_objects.object_type%type) is
select object_name
from user_objects
where object_name not like ('BIN$%') and object_name not like 'SYS_LOB%' 
and (object_name like 'BR_%' or object_name like 'BRFH%')
and object_type = q_object_type;
	
begin
	for i in object_types loop
		if i.object_type = 'TABLE' then
			for k in contrants loop					
				EXECUTE IMMEDIATE 'alter table ' || k.TABLE_NAME || ' drop constraint ' || k.CONSTRAINT_NAME;
			end loop;
			--drop tables;
			for l in tablelist(i.object_type) loop
				EXECUTE IMMEDIATE 'drop ' || i.object_type  || ' ' || l.object_name;
			end loop;
			
		elsif i.object_type in ('VIEW', 'TRIGGER','FUNCTION','PROCEDURE','PACKAGE','SEQUENCE') then	
			for p in tablelist(i.object_type) loop
				EXECUTE IMMEDIATE 'drop ' || i.object_type || ' ' || p.object_name;
			end loop;
		end if;
	end loop;
end;
/

PURGE recyclebin
/

