CREATE TABLE BRFHQZ_JOB_DETAILS
  (
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTIon VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB,
    CONSTRAINT PK_BRFHQZ_JOB_DETAILS PRIMARY KEY (JOB_NAME,JOB_GROUP) USING INDEX
);

CREATE TABLE BRFHQZ_JOB_LISTENERS
  (
    JOB_NAME  VARCHAR2(200) NOT NULL, 
    JOB_GROUP VARCHAR2(200) NOT NULL,
    JOB_LISTENER VARCHAR2(200) NOT NULL,
    CONSTRAINT PK_BRFHQZ_JOB_LISTENERS PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER) USING INDEX,
    CONSTRAINT FK_BRFHQZ_JOB_LNRS_JOB_DETAILS FOREIGN KEY (JOB_NAME,JOB_GROUP) REFERENCES BRFHQZ_JOB_DETAILS (JOB_NAME,JOB_GROUP)
);

CREATE TABLE BRFHQZ_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL, 
    JOB_GROUP VARCHAR2(200) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTIon VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB,
    CONSTRAINT PK_BRFHQZ_TRIGGERS PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP) USING INDEX,
    CONSTRAINT FK_BRFHQZ_TRIGGERS_JOB_DETAILS FOREIGN KEY (JOB_NAME,JOB_GROUP) REFERENCES BRFHQZ_JOB_DETAILS(JOB_NAME,JOB_GROUP) 
);

CREATE TABLE BRFHQZ_SIMPLE_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(10) NOT NULL,
    CONSTRAINT PK_BRFHQZ_SIMPLE_TRIGGERS PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP) USING INDEX,
    CONSTRAINT FK_BRFHQZ_SIMPLE_TRG_TRIGGER FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES BRFHQZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE BRFHQZ_CRON_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSIon VARCHAR2(120) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    CONSTRAINT PK_BRFHQZ_CRON_TRIGGERS PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP) USING INDEX,
    CONSTRAINT FK_BRFHQZ_CRON_TRG_TRIGGERS FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES BRFHQZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE BRFHQZ_BLOB_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB,
    CONSTRAINT PK_BRFHQZ_BLOB_TRIGGERS PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP) USING INDEX,
    CONSTRAINT FK_BRFHQZ_BLOB_TRG_TRIGGERS FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES BRFHQZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE BRFHQZ_TRIGGER_LISTENERS
  (
    TRIGGER_NAME  VARCHAR2(200) NOT NULL, 
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(200) NOT NULL,
    CONSTRAINT PK_BRFHQZ_TRIGGER_LISTENERS PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER) USING INDEX,
    CONSTRAINT FK_BRFHQZ_TRG_LSNT_TRIGGERS FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES BRFHQZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE BRFHQZ_CALENDARS
  (
    CALENDAR_NAME  VARCHAR2(200) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    CONSTRAINT PK_BRFHQZ_CALENDARS PRIMARY KEY (CALENDAR_NAME) USING INDEX
);

CREATE TABLE BRFHQZ_PAUSED_TRIGGER_GRPS
  (
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL, 
    CONSTRAINT PK_BRFHQZ_PAUSED_TRIGGER_GRPS PRIMARY KEY (TRIGGER_GROUP) USING INDEX
);

CREATE TABLE BRFHQZ_FIRED_TRIGGERS 
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    CONSTRAINT PK_BRFHQZ_FIRED_TRIGGERS PRIMARY KEY (ENTRY_ID) USING INDEX
);

CREATE TABLE BRFHQZ_SCHEDULER_STATE 
  (
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    CONSTRAINT PK_BRFHQZ_SCHEDULER_STATE PRIMARY KEY (INSTANCE_NAME) USING INDEX
);

CREATE TABLE BRFHQZ_LOCKS
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    CONSTRAINT PK_BRFHQZ_LOCKS PRIMARY KEY (LOCK_NAME) USING INDEX
);

