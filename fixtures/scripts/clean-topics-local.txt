//to clean

docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name notStandardisedFeedFixture
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name aggregatedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name aggregatedFixturesRecovery
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name notMappedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name whFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name brLcooRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name brLivescoutRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name runningballRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name tipexRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name ipsdRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name categoryMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name competitionMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name competitorMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name activeFeed
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name whCategories
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name whCompetitions
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config retention.ms=1 --entity-name whCompetitors

docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name notStandardisedFeedFixture
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name aggregatedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name aggregatedFixturesRecovery
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name notMappedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name whFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name brLcooRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name brLivescoutRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name runningballRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name tipexRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name ipsdRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name categoryMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name competitionMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name competitorMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name activeFeed
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name whCategories
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name whCompetitions
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config cleanup.policy --entity-name whCompetitors

//to check
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic notStandardisedFeedFixture --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic aggregatedFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic aggregatedFixturesRecovery --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic notMappedFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic whFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic brLcooRawFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic brLivescoutRawFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic runningballRawFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic tipexRawFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic ipsdRawFixtures --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic categoryMappings --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic competitionMappings --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic competitorMappings --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic activeFeed --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic whCategories --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic whCompetitions --property key.separator=: --property print.key=true --from-beginning
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic whCompetitors --property key.separator=: --property print.key=true --from-beginning

//to return back

docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name notStandardisedFeedFixture
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name aggregatedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name aggregatedFixturesRecovery
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name notMappedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name whFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name brLcooRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name brLivescoutRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name runningballRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name tipexRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name ipsdRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name categoryMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name competitionMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name competitorMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name activeFeed
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name whCategories
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name whCompetitions
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --add-config cleanup.policy=compact --entity-name whCompetitors

docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name notStandardisedFeedFixture
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name aggregatedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name aggregatedFixturesRecovery
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name notMappedFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name whFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name brLcooRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name brLivescoutRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name runningballRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name tipexRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name ipsdRawFixtures
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name categoryMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name competitionMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name competitorMappings
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name activeFeed
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name whCategories
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name whCompetitions
docker exec -ti kafka_kafka_1 /opt/kafka_2.12-0.11.0.0/bin/kafka-configs.sh --zookeeper zookeeper:2181 --entity-type topics --alter --delete-config retention.ms --entity-name whCompetitors