#!/usr/bin/env bash

echo -n "Enter name of topic to push messages in: "
read topic

docker exec -it kafka_kafka_1 /usr/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic $topic