#!/usr/bin/env bash

echo -n "Enter name of topic to subscribe on: "
read topic

docker exec -it kafka_kafka_1 /usr/bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic $topic