#!/usr/bin/env bash

echo -n "Enter name of topic to describe: "
read topic

docker exec -it kafka_kafka_1 /opt/usr/bin/kafka-topics.sh --describe $topic --zookeeper zookeeper:2181