#!/usr/bin/env bash

echo -n "Enter name of topic to create: "
read topic

docker exec -it kafka_kafka_1 /usr/bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic $topic