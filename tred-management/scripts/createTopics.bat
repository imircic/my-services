echo off
set zookeeper=%1
shift
shift
if not defined zookeeper (
   set zookeeper="zookeeper:2181"
   ECHO No zookeeper location provided. Using default value zookeeper:2181
)

call docker exec -it kafka_kafka_1 /opt/kafka_2.11-0.10.0.1/bin/kafka-topics.sh --create --zookeeper %zookeeper% --replication-factor 1 --partitions 10 --topic tred_fixtures --config cleanup.policy=compact --config segment.ms=10000
call docker exec -it kafka_kafka_1 /opt/kafka_2.11-0.10.0.1/bin/kafka-topics.sh --create --zookeeper %zookeeper% --replication-factor 1 --partitions 10 --topic tred_draws --config cleanup.policy=compact --config segment.ms=10000
call docker exec -it kafka_kafka_1 /opt/kafka_2.11-0.10.0.1/bin/kafka-topics.sh --create --zookeeper %zookeeper% --replication-factor 1 --partitions 10 --topic tred_matchfinished --config cleanup.policy=compact --config segment.ms=10000
call docker exec -it kafka_kafka_1 /opt/kafka_2.11-0.10.0.1/bin/kafka-topics.sh --create --zookeeper %zookeeper% --replication-factor 1 --partitions 10 --topic tred_tournaments --config cleanup.policy=compact --config segment.ms=10000
call docker exec -it kafka_kafka_1 /opt/kafka_2.11-0.10.0.1/bin/kafka-topics.sh --create --zookeeper %zookeeper% --replication-factor 1 --partitions 10 --topic tred_incidents --config cleanup.policy=compact --config segment.ms=10000
:END